let obj= require('./object.cjs');

function mapObject(obj,cb){
    for(let key in obj){
        let newvalue = obj[key];
        let val=cb(newvalue);
        obj[key]=val;

    }
    return obj;
}



module.exports=mapObject;
